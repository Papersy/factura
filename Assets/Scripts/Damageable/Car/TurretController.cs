﻿using System.Collections.Generic;
using DG.Tweening;
using Game;
using Infrastructure.Input;
using Infrastructure.Services;
using UnityEngine;

namespace Damageable.Car
{
    public class TurretController : MonoBehaviour
    {
        [SerializeField] private Bullet _bullet;
        [SerializeField] private Transform _bulletSpawnPoint;
        [SerializeField] private float _fireDelay;
        [SerializeField] private bool _isFire;

        private List<Bullet> _bulletsPool = new();
        private float _currentTime;
        private float _currentYRotation = 0f;
        private IInputService _inputService;
        
        private void Awake()
        {
            _inputService = AllServices.Container.Single<IInputService>();
            GenerateBulletsPool();
        }

        private void OnEnable()
        {
            _inputService.OnEventDown += StartShoot;
            _inputService.OnEventUp += StopShoot;
            _inputService.OnEventDrag += MoveTurret;
        }

        private void OnDisable()
        {
            _inputService.OnEventDown -= StartShoot;
            _inputService.OnEventUp -= StopShoot;
            _inputService.OnEventDrag -= MoveTurret;
        }

        private void Update()
        {
            if(!_isFire || !GameController.IsGame)
                return;
            
            if (_currentTime >= _fireDelay)
            {
                _currentTime = 0f;
                Shoot();
            }

            _currentTime += Time.deltaTime;
        }

        private void Shoot()
        {
            DOTween.Kill(GetHashCode());
            
            var sequence = DOTween.Sequence().SetId(GetHashCode());
            sequence
                .Append(transform.DOLocalMoveZ(-2f, 0.1f))
                .Append(transform.DOLocalMoveZ(0, 0.25f).SetEase(Ease.OutQuad))
                .OnComplete(() =>
                {
                    var bulletPos = _bulletSpawnPoint.transform.position;
                    var bulletRotation = Quaternion.Euler(0, _currentYRotation, 0);
                    // var bullet = Instantiate(_bullet, bulletPos, bulletRotation);
                    var bullet = GetFreeBullet();
                    bullet.transform.position = bulletPos;
                    bullet.transform.rotation = bulletRotation;
                    
                    bullet.StartMove();
                });
        }
        
        private void StartShoot()
        {
            _currentTime = _fireDelay;
            _isFire = true;
        }

        private void StopShoot()
        {
            _isFire = false;
        }
        
        private void MoveTurret(float value)
        {
            if(!GameController.IsGame)
                return;
            
            var newRotationValue = value / 35f + _currentYRotation;
            _currentYRotation = Mathf.Clamp(newRotationValue, -90f, 90f);
            transform.rotation = Quaternion.Euler(-90, _currentYRotation, 0);
        }

        private void GenerateBulletsPool()
        {
            for (int i = 0; i < 50; i++)
            {
                var bullet = Instantiate(_bullet);
                bullet.gameObject.SetActive(false);
                _bulletsPool.Add(bullet);
            }
        }

        private Bullet GetFreeBullet()
        {
            foreach (var bullet in _bulletsPool)
            {
                if (!bullet.gameObject.activeSelf)
                {
                    bullet.gameObject.SetActive(true);
                    return bullet;
                }
            }
            
            var newBullet = Instantiate(_bullet);
            _bulletsPool.Add(newBullet);

            return newBullet;
        }
    }
}