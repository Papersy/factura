﻿using Damageable.StickMan;
using UnityEngine;

namespace Damageable.Car
{
    public class CollisionInit : MonoBehaviour
    {
        [SerializeField] private Transform _car;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
                other.GetComponent<StickmanController>().StartMove(_car);
        }
    }
}