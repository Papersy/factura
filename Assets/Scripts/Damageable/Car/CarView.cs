﻿using UnityEngine;
using UnityEngine.UI;

namespace Damageable.Car
{
    public class CarView : MonoBehaviour
    {
        [SerializeField] private Slider _health;

        public void UpdateHealth(int value) => _health.value = value;
    }
}