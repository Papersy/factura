﻿using UnityEngine;

namespace Damageable.Car
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private TrailRenderer _trailRenderer;
        [SerializeField] private float _timeToDestroy;
        [SerializeField] private float _speed;
        [SerializeField] private int _damage;

        private float _currentTime;
        private bool _isMove = false;
        
        private void Update()
        {
            if(!_isMove)
                return;
            
            transform.Translate(Vector3.forward * _speed * Time.deltaTime);

            _currentTime += Time.deltaTime;
            if (_currentTime >= _timeToDestroy)
                DisableBullet();
        }

        public void StartMove()
        {
            _currentTime = 0f;
            _isMove = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IDamageable damageable))
            {
                damageable.GetDamage(_damage);
                DisableBullet();
            }
        }

        private void DisableBullet()
        {
            _isMove = false;
            gameObject.SetActive(false);
            _trailRenderer.Clear();
        }
    }
}