﻿using System;
using Game;
using UnityEngine;

namespace Damageable.Car
{
    public class CarController : MonoBehaviour, IDamageable
    {
        [SerializeField] private CarView _carView;
        [SerializeField] private int _health;
        [SerializeField] private float _speed = 50f;

        public void Update()
        {
            if(!GameController.IsGame)
                return;
            if(transform.position.z >= 800)
                GameController.Instance.Victory();
            
            transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        }

        public void GetDamage(int value)
        {
            _health -= value;
            _carView.UpdateHealth(_health);

            if (_health <= 0)
            {
                GameController.Instance.Lose();
            }
        }

        public void RestartLevel()
        {
            _health = 100;
            transform.position = Vector3.zero;
        }
    }
}