﻿using System;
using UnityEngine;

namespace Damageable.StickMan
{
    public class StickmanController : MonoBehaviour, IDamageable
    {
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private Animator _stickManAnimator;
        [SerializeField] private StickmanView _stickmanView;
        
        private StickmanModel _stickManModel;
        private Transform _target;

        private const string AnimationHashRun = "IsRun";

        private void Awake() => _stickManModel = new StickmanModel();

        private void Update()
        {
            if(!_stickManModel.IsMove)
                return;
            if(_target == null)
                return;
            
            Vector3 directionToTarget = _target.position - transform.position;
            
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget, Vector3.up);
            
            targetRotation.x = 0;
            targetRotation.z = 0;
            
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _stickManModel.RotationSpeed * Time.deltaTime);
            transform.Translate(Vector3.forward * _stickManModel.MoveSpeed * Time.deltaTime);
        }

        public void StartMove(Transform target)
        {
            _target = target;
            _stickManModel.IsMove = true;
            _stickManAnimator.SetBool(AnimationHashRun, true);
        }

        public void ResetParameters()
        {
            _stickManModel.ResetHealth();
            _stickmanView.UpdateHealth(_stickManModel.Health);
            _stickManAnimator.SetBool(AnimationHashRun, false);
            _stickManModel.IsMove = false;
        }
        
        public void GetDamage(int damage)
        {
            if (_particleSystem.isPlaying)
            {
                _particleSystem.Stop();
                _particleSystem.Play();
            }
            else 
                _particleSystem.Play();
            
            _stickManModel.Health -= damage;
            _stickmanView.UpdateHealth(_stickManModel.Health);
            
            if (_stickManModel.Health <= 0)
            {
                _stickManAnimator.SetBool(AnimationHashRun, false);
                _stickManModel.IsMove = false;
                gameObject.SetActive(false);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                other.GetComponent<IDamageable>().GetDamage(_stickManModel.Damage);
                gameObject.SetActive(false);
            }
        }
    }
}