﻿namespace Damageable.StickMan
{
    public class StickmanModel
    {
        public int Damage = 3;
        public int Health = 100;
        public float MoveSpeed = 10f;
        public float RotationSpeed = 3f;
        public bool IsMove = false;

        public void ResetHealth() => Health = 100;
    }
}