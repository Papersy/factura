﻿using UnityEngine;
using UnityEngine.UI;

namespace Damageable.StickMan
{
    public class StickmanView : MonoBehaviour
    {
        [SerializeField] private Slider _health;
        
        public void UpdateHealth(int health)
        {
            _health.value = health;

            if (health >= 100)
                _health.gameObject.SetActive(false);
            else
                _health.gameObject.SetActive(true);
        }
    }
}