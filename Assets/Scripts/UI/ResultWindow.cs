﻿using System;
using Infrastructure.Services.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ResultWindow : BaseWindow
    {
        [SerializeField] private Button _restart;
        [SerializeField] private TextMeshProUGUI _textResult;
        
        public event Action OnRestart;

        private void OnEnable() => _restart.onClick.AddListener(Restart);
        private void OnDisable() => _restart.onClick.RemoveListener(Restart);

        public void Lose()
        {
            Show();

            _textResult.text = "You lose :(";
        }

        public void Victory()
        {
            Show();

            _textResult.text = "You win!";
        }

        private void Restart()
        {
            OnRestart?.Invoke();
            
            Hide();
        }
    }
}