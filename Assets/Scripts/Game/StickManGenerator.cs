﻿using System;
using System.Collections.Generic;
using Damageable.Car;
using Damageable.StickMan;
using UnityEngine;
using Random = UnityEngine.Random;
using Task = System.Threading.Tasks.Task;

namespace Game
{
    public class StickManGenerator : MonoBehaviour
    {
        [SerializeField] private CarController _carController;
        [SerializeField] private StickmanController _stickMan;
        [SerializeField] private float _spawnDelay;

        private List<StickmanController> _stickMansPool = new();
        private float _currentTime;

        private void OnEnable()
        {
            
        }

        private void OnDisable()
        {
            
        }

        private void Update()
        {
            if(!GameController.IsGame)
                return;
            
            if (_currentTime > _spawnDelay)
            {
                SpawnStickMan();
                _currentTime = 0f;
            }

            _currentTime += Time.deltaTime;
        }

        public async Task InitGeneration()
        {
            const int amountStickMansToGenerate = 30;
            for (int i = 0; i < amountStickMansToGenerate; i++)
            {
                var stickMan = Instantiate(_stickMan);
                stickMan.gameObject.SetActive(false);
                _stickMansPool.Add(stickMan);
            }

            GenerateFirstStickMans();
        }

        private void SpawnStickMan()
        {
            var carPos = _carController.transform.position;

            var randomX = Random.Range(-10, 10);
            var randomZ = Random.Range(50, 150);

            var spawnPos = new Vector3(carPos.x + randomX, 0f, carPos.z + randomZ);
            var stickMan = GetFreeStickMan();
            stickMan.transform.position = spawnPos;

            var randomRotate = Random.Range(160f, 200f);
            stickMan.transform.rotation = Quaternion.Euler(0, randomRotate, 0);
            stickMan.ResetParameters();
        }

        private StickmanController GetFreeStickMan()
        {
            foreach (var stickMan in _stickMansPool)
            {
                if (!stickMan.gameObject.activeSelf)
                {
                    stickMan.gameObject.SetActive(true);
                    return stickMan;
                }
            }
            
            var newStickMan = Instantiate(_stickMan);
            newStickMan.gameObject.SetActive(false);
            _stickMansPool.Add(newStickMan);

            return newStickMan;
        }

        public void GenerateFirstStickMans()
        {
            for (int i = 0; i < 5; i++)
                SpawnStickMan();
        }

        public void RestartLevel()
        {
            foreach (var stickMan in _stickMansPool)
                stickMan.gameObject.SetActive(false);
        }
    }
}