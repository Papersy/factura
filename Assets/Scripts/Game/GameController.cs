﻿using Damageable.Car;
using Infrastructure.Input;
using Infrastructure.Services;
using Infrastructure.Services.UI;
using UnityEngine;

namespace Game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private CameraController _cameraController;
        [SerializeField] private CarController _carController;
        [SerializeField] private StickManGenerator _stickManGenerator;
        
        public static bool IsGame = false;
        public static GameController Instance;
        
        private IInputService _inputService;
        private GameUICanvas _gameUICanvas;
        
        private void Awake()
        {
            Instance = this;
            
            _inputService = AllServices.Container.Single<IInputService>();
            _gameUICanvas = AllServices.Container.Single<IUIService>().HudContainer.GameCanvas;
        }
        
        private void OnEnable()
        {
            _inputService.OnEventDown += StartGame;
            _gameUICanvas.ResultWindow.OnRestart += RestartLevel;
        }

        private void OnDisable()
        {
            _inputService.OnEventDown -= StartGame;
            _gameUICanvas.ResultWindow.OnRestart -= RestartLevel;
        }

        private void StartGame()
        {
            _cameraController.EnableFollowCamera(() => IsGame = true);
        }

        public void Lose()
        {
            _cameraController.EnableStaticCamera();
            _carController.RestartLevel();
            
            IsGame = false;
            _gameUICanvas.ResultWindow.Lose();
        }

        public void Victory()
        {
            _cameraController.EnableStaticCamera();
            _carController.RestartLevel();
            
            IsGame = false;
            _gameUICanvas.ResultWindow.Victory();
        }

        private void RestartLevel()
        {
            _stickManGenerator.RestartLevel();
            _stickManGenerator.GenerateFirstStickMans();
        }
    }
}