﻿using System;
using System.Collections;
using Cinemachine;
using UnityEngine;

namespace Game
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _staticCamera;
        [SerializeField] private CinemachineVirtualCamera _followCamera;
        
        public void EnableFollowCamera(Action action) => StartCoroutine(FollowCameraAnimation(action));

        public void EnableStaticCamera()
        {
            _staticCamera.enabled = true;
        }

        private IEnumerator FollowCameraAnimation(Action action)
        {
            _followCamera.enabled = true;
            _staticCamera.enabled = false;
            
            yield return new WaitForSeconds(2f);
            
            action?.Invoke();
        }
    }
}