﻿using CodeBase.StaticData;
using Data;
using Infrastructure.AssetManagement;
using Infrastructure.Factory;
using Infrastructure.Input;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using Infrastructure.Services.UI;
using UnityEngine;

namespace Infrastructure.States
{
    public class BootstrapState : IState
    {
        private const string Initial = "Boot";
        private const string MainScene = "MainScene";

        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _services;

        public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader, AllServices services)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _services = services;

            RegisterServices();
        }

        public void Enter() =>
            _sceneLoader.Load(Initial, onLoaded: EnterLoadLevel);

        public void Exit()
        { }

        private void EnterLoadLevel() =>
            _stateMachine.Enter<LoadLevelState, string>(MainScene);

        private void RegisterServices()
        {
            _services.RegisterSingle<IStaticDataService>(new StaticDataService());
            _services.RegisterSingle<IDataService>(new DataService());
            _services.RegisterSingle<IGameStateMachine>(_stateMachine);
            _services.RegisterSingle<IAssetProvider>(new AssetProvider());
            _services.RegisterSingle<IGameFactory>(new GameFactory(_services.Single<IAssetProvider>()));
            
            GameObject hudContainerGO = _services.Single<IGameFactory>().Instantiate(ConstantsData.HUDAddress);
            HUDContainer hudContainer = hudContainerGO.GetComponent<HUDContainer>();
            
            _services.RegisterSingle<IUIService>(new UIService(hudContainer));
            _services.RegisterSingle(_services.Single<IUIService>()
                .HudContainer.InputService.GetComponent<IInputService>());
        }
    }
}