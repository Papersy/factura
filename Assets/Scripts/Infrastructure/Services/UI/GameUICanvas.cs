﻿using UI;
using UnityEngine;

namespace Infrastructure.Services.UI
{
    public class GameUICanvas : MonoBehaviour
    {
        public ResultWindow ResultWindow;
        
        public static GameUICanvas Instance;

        public void Awake() => Instance = this;
    }
}