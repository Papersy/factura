﻿using Infrastructure.Input;
using UnityEngine;

namespace Infrastructure.Services.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class HUDContainer : MonoBehaviour
    {
        public InputService InputService;
        public GameUICanvas GameCanvas;
        
        private CanvasGroup canvasGroup;
        
        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            DontDestroyOnLoad(this);
        }
    }
}