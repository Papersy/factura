namespace Infrastructure.Services.UI
{
    public interface IUIService : IService
    {
        HUDContainer HudContainer { get; }
    }
}